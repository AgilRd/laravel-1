<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/selamat" method="post">
        @csrf
        <label for="">First name: </label> <br> <br>
        <input type="text" name="first">  <br> <br>
        <label for="">Last name:  </label> <br> <br>
        <input type="text" name="last"> <br> <br>
        <label for="">Gender: </label> <br> <br>
        <input type="radio" name="gender" id=""> Male <br>
        <input type="radio" name="gender" id=""> Female <br>
        <input type="radio" name="gender" id=""> Other <br> <br>
        <label for="">Nationality: </label> <br> <br>
        <select name="nationality" id="">
            <option value="indonesia">Indonesian</option>
            <option value="singapore">Singaporean</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select> <br> <br>
        <label for="">Language Spoken</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br> 
        <input type="checkbox"> Bahasa Inggris <br> 
        <input type="checkbox"> Other <br> <br>
        <label for="">Bio: </label> <br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up" >
    </form>
</body>
</html>