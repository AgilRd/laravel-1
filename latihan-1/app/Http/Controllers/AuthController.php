<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('register');
    }

    public function data(Request $request){
        $nama = $request['first'];
        $nama2 = $request['last'];

        return view('selamat', compact('nama', 'nama2'));
    }
}
